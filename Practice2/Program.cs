﻿namespace Practice2;
class Program
{
    static void Main(string[] args)
    {
        //First Challenge
        string fullName = "Petrov Ivan Sergeevich";
        int age = 34;
        string email = "petrivan@gmail.com";
        float progScore = 92.5f;
        float mathScore = 85.2f;
        float physScore = 79.3f;

        Console.WriteLine($"Full Name: {fullName}");
        Console.WriteLine($"Age: {age}");
        Console.WriteLine($"E-Mail: {email}");
        Console.WriteLine($"Programming scores: {progScore}");
        Console.WriteLine($"Math scores: {mathScore}");
        Console.WriteLine($"Physics scores: {physScore}");
        Console.ReadKey();

        //Second Challenge
        float sumScore;
        sumScore = progScore + mathScore + physScore;
        float averageScore;
        averageScore = sumScore / 3;
        Console.WriteLine($"Scores for all subjects: {sumScore}");
        Console.WriteLine($"Average score in all subjects: {averageScore}");
        Console.ReadKey();
    }
}